import { App } from 'aws-cdk-lib';
import { CdnStack } from './stack';

const app = new App();

new CdnStack(app, 'CdnStack', {
    stackName: 'Cdn',
    description: 'Public Static CDN backed by S3',

    env: {
        account: process.env.CDK_DEFAULT_ACCOUNT,
        region: process.env.CDK_DEFAULT_REGION
    }
});
