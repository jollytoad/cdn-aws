import { CfnOutput } from 'aws-cdk-lib';
import { DnsValidatedCertificate, ICertificate } from 'aws-cdk-lib/aws-certificatemanager';
import { AllowedMethods, CachedMethods, CachePolicy, Distribution, IDistribution, OriginRequestPolicy, ViewerProtocolPolicy } from 'aws-cdk-lib/aws-cloudfront';
import { S3Origin } from 'aws-cdk-lib/aws-cloudfront-origins';
import { ARecord, HostedZone, IHostedZone, IRecordSet, RecordTarget } from 'aws-cdk-lib/aws-route53';
import { CloudFrontTarget } from 'aws-cdk-lib/aws-route53-targets';
import { Bucket, BucketProps, HttpMethods, IBucket } from 'aws-cdk-lib/aws-s3';
import { BucketDeployment, ISource } from 'aws-cdk-lib/aws-s3-deployment';
import { Construct } from 'constructs';

export interface StaticCdnProps {
    /**
     * Full domain name for the CDN site
     */
    readonly domainName: string

    /**
     * A pre-existing certificate.
     * If not supplied then a new one will be requested for the domainName,
     * or the certificateDomain is supplied.
     */
    readonly certificate?: ICertificate

    /**
     * Should a wildcard certificate be created?
     */
    readonly wildcardCertificate?: boolean

    /**
     * Additional bucket properties
     */
    readonly bucketProps?: BucketProps

    /**
     * Sources to be deployed into the bucket
     */
    readonly sources?: ISource[]
}

export class StaticCdn extends Construct {
    private readonly domainName: string;
    private readonly zone: IHostedZone;

    readonly certificate: ICertificate;
    readonly bucket: IBucket;
    readonly distribution: IDistribution;
    readonly dnsRecord: IRecordSet;

    constructor(scope: Construct, id: string, props: StaticCdnProps) {
        super(scope, id);

        this.domainName = props.domainName;

        this.zone = this.lookupZone()

        this.certificate = props.certificate ?? this.buildCertificate(props.wildcardCertificate ?? false)

        this.bucket = this.buildBucket(props.bucketProps);

        this.distribution = this.buildDistribution();

        this.dnsRecord = this.buildDnsRecord();

        if (props.sources) {
            this.buildDeployment(props.sources);
        }
    }

    private lookupZone(): IHostedZone {
        return HostedZone.fromLookup(this, 'Zone', {
            domainName: this.domainName.replace(/^[\w]+\./, '')
        })
    }

    private buildBucket(props: BucketProps = {}): IBucket {
        return new Bucket(this, 'Bucket', {
            publicReadAccess: true,
            cors: [
                {
                    allowedHeaders: ['*'],
                    allowedMethods: [HttpMethods.GET, HttpMethods.HEAD],
                    allowedOrigins: ['*']
                }
            ],
            // IMPORTANT: DO NOT REMOVE THIS
            // This is required to enable website hosting and allow S3 objects to perform redirections
            // which is critical for import-map redirections. 
            websiteIndexDocument: 'index.html',
            ...props
        });
    }

    private buildCertificate(wildcard: boolean) {
        const domainName = wildcard ? this.domainName.replace(/^[\w]+\./, '*.') : this.domainName
        return new DnsValidatedCertificate(this, 'Certificate', {
            domainName,
            hostedZone: this.zone,
            // NOTE: We always have to use 'us-east-1' region to create the certificate regardless of the default region
            region: 'us-east-1'
        })
    }

    private buildDistribution(): IDistribution {
        const origin = new S3Origin(this.bucket);

        return new Distribution(this, 'Distribution', {
            defaultBehavior: {
                origin,
                allowedMethods: AllowedMethods.ALLOW_GET_HEAD_OPTIONS,
                cachedMethods: CachedMethods.CACHE_GET_HEAD_OPTIONS,
                cachePolicy: CachePolicy.CACHING_OPTIMIZED,
                originRequestPolicy: OriginRequestPolicy.CORS_S3_ORIGIN,
                viewerProtocolPolicy: ViewerProtocolPolicy.HTTPS_ONLY,
            },
            certificate: this.certificate,
            domainNames: [this.domainName]
        });
    }

    private buildDnsRecord(): IRecordSet {
        const target = RecordTarget.fromAlias(new CloudFrontTarget(this.distribution));
        
        return new ARecord(this, 'AliasRecord', {
            recordName: this.domainName.split('.',1)[0],
            zone: this.zone,
            target
        });
    }

    private buildDeployment(sources: ISource[]) {
        new BucketDeployment(this, 'BucketDeployment', {
            sources,
            destinationBucket: this.bucket,
            // IMPORTANT: Leave as false, DO NOT prune existing content when redeploying
            prune: false
        })
    }

    public createExports(prefix: string) {
        new CfnOutput(this, 'Url', {
            exportName: `${prefix}::URL`,
            description: 'CDN Url',
            value: `https://${this.dnsRecord.domainName}`
        });

        new CfnOutput(this, 'BucketName', {
            exportName: `${prefix}::Bucket::Name`,
            description: 'CDN Public Assets Bucket Name',
            value: this.bucket.bucketName
        });

        new CfnOutput(this, 'BucketArn', {
            exportName: `${prefix}::Bucket::ARN`,
            description: 'CDN Public Assets Bucket ARN',
            value: this.bucket.bucketArn
        });

        new CfnOutput(this, 'CertificateArn', {
            exportName: `${prefix}::Certificate::ARN`,
            description: 'CDN Certificate ARN',
            value: this.certificate.certificateArn
        });

        new CfnOutput(this, 'DistributionDomainName', {
            exportName: `${prefix}::Distribution::DomainName`,
            description: 'CDN CloudFront Distribution domain name',
            value: this.distribution.distributionDomainName
        });

        new CfnOutput(this, 'DistributionId', {
            exportName: `${prefix}::Distribution::Id`,
            description: 'CDN CloudFront Distribution Id',
            value: this.distribution.distributionId
        });
    }
}
