import { RemovalPolicy, Stack, StackProps } from 'aws-cdk-lib';
import { Source } from 'aws-cdk-lib/aws-s3-deployment';
import { Construct } from 'constructs';
import { StaticCdn } from './static-cdn';

export class CdnStack extends Stack {
    constructor(scope: Construct, id: string, props: StackProps) {
        super(scope, id, props);

        const cdn = new StaticCdn(this, 'PublicCdn', {
            domainName: 'cdn.mgibson.staff.adaptavist.com',
            wildcardCertificate: true,

            sources: [Source.asset('./site')],

            bucketProps: {
                removalPolicy: RemovalPolicy.DESTROY,
                // autoDeleteObjects: true
            }
        })

        cdn.createExports('PublicCdn')
    }
}
