# CDN in AWS

A simple Content Delivery Network, using AWS CloudFront and S3.

# Deployment to personal dev account

Edit the props in `stack/index.ts`, adjusting at least the `domain`.

$ aws-vault exec $AWS_PROFILE -- yarn deploy
